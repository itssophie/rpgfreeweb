# RPG freeformat converting

Fixed to free converter written in Node.js JavaScript which you can self host. The port number is currently hardcoded to `9124`.

### Setup

1. clone git repo
2. `npm i`
3. `node server` to run the server
4. access http://ip or localhost:9124

### Issues

If something isn't converting, make an issue or even better.. make a PR :)
